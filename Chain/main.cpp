#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <filesystem>

#include "chain_file.hpp"
#include "chain_project_file.hpp"

namespace fs = std::filesystem;

std::string parse(std::string line)
{
	return "nothing";
}

std::vector<fs::path> get_files_with_extension(std::string extension, fs::path path)
{
	std::vector<fs::path> files = {};
	if (!fs::exists(path) || !fs::is_directory(path))
	{
		return files;
	}

	for (fs::directory_iterator it(path), endit;
		it != endit; ++it)
	{
		if (fs::is_regular_file(*it) && it->path().extension() == extension)
		{
			files.push_back(it->path().filename());
		}
	}

	return files;
}

int main()
{
	fs::path current_path = fs::current_path();
	//fs::create_directory(currentpath / "chaindatapack");
	//fs::remove(currentpath / "chaindatapack");
	chain_project_file project = { get_files_with_extension(".chainproj", current_path).at(0) };
	//std::cout << "---------\n" << project.get_path() << std::endl;

	std::vector<chain_file> chain_files = {};
	for (auto p : get_files_with_extension(".chain", current_path))
	{
		chain_files.push_back(chain_file(p));
		//std::cout << chainfiles.at(chainfiles.size() - 1).get_path() << std::endl;
	}

	// Setup Datapack
	fs::remove_all(current_path / project.settings.datapack_name);
	fs::create_directories(current_path / project.settings.datapack_name / "data");

	// Create pack.mcmeta
	fs::path path_to_pack_meta = current_path / project.settings.datapack_name / "pack.mcmeta";
	std::ofstream pack_meta(path_to_pack_meta.c_str());
	pack_meta << "{\n	\"pack\":\n	{\n		\"pack_format\": 1,\n		\"description\": \"" << project.settings.datapack_name << "\"\n	}\n}";

	for (auto& f : chain_files)
	{
		std::ifstream file(f.get_path().c_str());
		if (!file.is_open())
		{
			throw "File doesn't exist!";
			return 1;
		}
		fs::create_directories(current_path / project.settings.datapack_name / "data" / f.settings.name_space / "functions");
		std::string fname = f.settings.name + ".mcfunction";
		fs::path p = current_path / project.settings.datapack_name / "data" / f.settings.name_space / "functions" / fname;
		std::ofstream output(p.c_str());
		std::cout << p << std::endl;
		while (!file.eof() && !file.fail())
		{
			std::string line;
			std::getline(file, line);
			if (line[0] != '#' && line != "")
			{
				std::cout << line << std::endl;
				output << line << "\n";
			}
		}
	}
}
