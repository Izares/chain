#pragma once

#include <filesystem>
#include <string>
#include <vector>
#include <fstream>

namespace fs = std::filesystem;

struct function_settings
{
	std::string name;
	std::string name_space;
	bool on_tick;
	bool on_load;
	std::vector<std::string> tags;
};

class chain_file
{
public:
	chain_file(fs::path p);
	const fs::path& get_path() const;

	function_settings settings;
private:
	std::ifstream file;
	fs::path path_to_file_;
};