#include <iostream>

#include "chain_file.hpp"
#include "string_utils.hpp"

chain_file::chain_file(fs::path p)
	: file(p.c_str())
	, path_to_file_(std::move(p))
{
	std::cout << path_to_file_;

	// Check if file is exists.
	if (!file.is_open())
	{
		// If it doesn't stop the program.
		throw "File doesn't exist!";
		return;
	}
	// Loop through the lines
	while (!file.eof() && !file.fail())
	{
		std::string line;
		std::getline(file, line);
		if (!line[0] == '#' || line == "")
		{
			return;
		}
		// Split
		std::vector<std::string> splitted = split(line, ':');
		// Store data
		if (splitted.at(0) == "#namespace")
		{
			settings.name_space = splitted.at(1);
			ltrim(settings.name_space);
			std::cout << settings.name_space << std::endl;
		}
		if (splitted.at(0) == "#name")
		{
			settings.name = splitted.at(1);
			ltrim(settings.name);
			std::cout << settings.name << std::endl;
		}
		if (splitted.at(0) == "#tag")
		{
			settings.tags.push_back(splitted.at(1));
			ltrim(settings.tags.at(settings.tags.size()-1));
		}
	}
}

const fs::path& chain_file::get_path() const
{
	return path_to_file_;
}