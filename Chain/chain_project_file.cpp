#include <iostream>

#include "chain_project_file.hpp"
#include "string_utils.hpp"

// Move the trim stuff, and split. It doesn't belong here



// This is the end of the stuff needed to copy out

chain_project_file::chain_project_file(fs::path p)
	: path_to_file_(std::move(p))
	, file(path_to_file_.c_str())
{
	// Check if file is exists.
	if (!file.is_open())
	{
		// If it doesn't stop the program.
		throw "File doesn't exist!";
		return;
	}
	// Loop through the lines
	while (!file.eof() && !file.fail())
	{
		std::string line;
		std::getline(file, line);
		// Split
		std::vector<std::string> splitted = split(line, ':');
		// Store data
		if (splitted.at(0) == "abrev")
		{
			settings.abrev = splitted.at(1);
			ltrim(settings.abrev);
		}
		if (splitted.at(0) == "description")
		{
			settings.description = splitted.at(1);
			ltrim(settings.description);
		}
		if (splitted.at(0) == "datapack_name")
		{
			settings.datapack_name = splitted.at(1);
			ltrim(settings.datapack_name);
		}
	}
}

const fs::path& chain_project_file::get_path() const
{
	return path_to_file_;
}