#pragma once

#include <fstream>
#include <filesystem>
#include <string>

namespace fs = std::filesystem;

struct project_settings
{
	std::string abrev;
	std::string description;
	std::string datapack_name;
};

class chain_project_file
{
public:
	chain_project_file(fs::path p);
	const fs::path& get_path() const;

	project_settings settings;
private:
	fs::path path_to_file_;
	std::ifstream file;
};